var app = angular.module('starter', ['ionic', 'ngCordova', 'ngCordovaOauth', 'ngResource', 'ngStorage']);

app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
});

app.constant("config", {
    "version": "1.0.0",
    "apiEndpoint": "https://corate-backend.herokuapp.com",
    //"apiEndpoint": "http://localhost:5000",
    "tw": {
        "apiKey": "Xnd7qgM9n1epACsXQdzyLkQpB",
        "apiSecret": "OV1ufMj6GCbmWI87ghsgrQOvKsmSNT2WYGkygkcc9MtpJxIfu5"
    },
    "pocket": {
        "consumerKey": "46552-7a2ca9fac9dd1e0feb3fe4a0"
    }
});

app.config(['$resourceProvider', '$httpProvider', '$stateProvider', '$urlRouterProvider', function ($resourceProvider, $httpProvider, $stateProvider, $urlRouterProvider) {
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $resourceProvider.defaults.stripTrailingSlashes = false;

    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'templates/home.html'
    }).state('articles', {
        url: '/articles',
        templateUrl: 'templates/articles.html'
    });
    $urlRouterProvider.otherwise('/');
}]);

app.factory('api', function($q, $http, config) {

    return {
        authenticate: function(data) {
            var deferred = $q.defer();
            $http({
                method: "post",
                url: config.apiEndpoint + '/api/v1/authenticate',
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                data: data
            })
            .then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        },
        articles: function(auth) {
            var deferred = $q.defer();
            $http({
                method: "get",
                url: config.apiEndpoint + '/api/v1/users/' + auth['userID'] + '/articles',
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "authToken": auth['accessToken']
                },
            })
            .then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        },
        process: function(auth) {
            var deferred = $q.defer();
            $http({
                method: "post",
                url: config.apiEndpoint + '/api/v1/users/' + auth['userID'] + '/process',
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "authToken": auth['accessToken']
                },
            })
            .then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        }
    }

});

app.controller("ExampleController", function($scope, $cordovaOauth, $state, $localStorage, api, config) {

    $scope.version = config.version;
    $scope.pocket = {};
    $scope.twitter = {};
    $scope.user = {};
    $scope.status = "Waiting";
    $scope.auth = {};
    $scope.$storage = $localStorage;

    $scope.testLogin = function() {
        $state.go('articles');
    };

    $scope.twitterLogin = function(){
        $cordovaOauth.twitter(config.tw.apiKey, config.tw.apiSecret).then(function(result) {
            var data = result;
            $scope.status = "Success";
            $scope.twitter.data = data;
            var options = {
                'networkID': data['user_id'],
                'network': 'TW',
                'accessToken': data['oauth_token'],
                'secretToken': data['oauth_token_secret']
            }
            api.authenticate(options).then(function(response) {
                $scope.status = "Authenticate success, loading article page, please wait!";
                $scope.auth = response.data;
                $scope.$storage.auth = response.data;

                $state.go('articles');

            }, function(response) {
                $scope.status = "Authenticate failed";
            });
        }, function(error) {
            $scope.status = "Failed";
            $scope.twitter.data = JSON.stringify(error);
        });
    };

    $scope.pocketLogin = function(){
        console.log("Pocket login");
        $cordovaOauth.pocket(config.pocket.consumerKey).then(function(result) {
            var data = result.split("&");
            $scope.pocket.accessToken = data[0].split("access_token=")[1];
            $scope.pocket.username = decodeURIComponent(data[1].split("username=")[1]);
            $scope.status = "Success";

            var options = {
                'networkID': $scope.pocket.username,
                'network': 'POCKET',
                'accessToken': $scope.pocket.accessToken,
                'secretToken': config.pocket.consumerKey
            }
            api.authenticate(options).then(function(response) {
                $scope.status = "Authenticate success, loading article page, please wait!";
                $scope.auth = response.data;
                $scope.$storage.auth = response.data;

                $state.go('articles');

            }, function(response) {
                $scope.pocket.status = "Authenticate failed";
            });

        }, function(error) {
            $scope.status = "Failed";
            $scope.pocket.data = JSON.stringify(error);
        });
    };

    $scope.getArticles = function() {
        api.articles($scope.$storage.auth).then(function(response) {
            $scope.status = "Get articles success";
            $scope.articles = response.data;
        }, function(response) {
            $scope.status = "Get articles failed" + response;
        });
    }

    $scope.process = function() {
        api.process($scope.$storage.auth).then(function(response) {
            $scope.status = "Process success";
            $scope.articles = response.data;
        }, function(response) {
            $scope.status = "Process failed" + response;
        });
    }

});
